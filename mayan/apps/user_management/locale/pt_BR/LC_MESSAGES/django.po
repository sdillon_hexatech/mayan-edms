# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Aline Freitas <aline@alinefreitas.com.br>, 2016
# Emerson Soares <on.emersonsoares@gmail.com>, 2013
# Jadson Ribeiro <jadsonbr@outlook.com.br>, 2017
# Renata Oliveira <renatabels@gmail.com>, 2011
# Roberto Rosario, 2012
# Rogerio Falcone <rogerio@falconeit.com.br>, 2015
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-10-29 12:45-0400\n"
"PO-Revision-Date: 2018-09-27 02:32+0000\n"
"Last-Translator: Roberto Rosario\n"
"Language-Team: Portuguese (Brazil) (http://www.transifex.com/rosarior/mayan-edms/language/pt_BR/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pt_BR\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: apps.py:53 permissions.py:7
msgid "User management"
msgstr "Gerenciar usuários"

#: apps.py:68
msgid "All the groups."
msgstr "Todos os grupos."

#: apps.py:72
msgid "All the users."
msgstr "Todos os usuários."

#: apps.py:90 links.py:33 links.py:57 links.py:78 views.py:274
msgid "Users"
msgstr "Usuários"

#: apps.py:94
msgid "Full name"
msgstr "Nome Completo"

#: apps.py:97 search.py:22
msgid "Email"
msgstr "E-mail"

#: apps.py:100
msgid "Active"
msgstr "Ativo"

#: apps.py:106
msgid "Has usable password?"
msgstr "tem senha usável?"

#: links.py:18 views.py:32
msgid "Create new group"
msgstr "Criar novo Grupo"

#: links.py:22 links.py:46 links.py:61
msgid "Delete"
msgstr "excluir"

#: links.py:25 links.py:49
msgid "Edit"
msgstr "Editar"

#: links.py:29 links.py:38 links.py:53 search.py:25 views.py:71
msgid "Groups"
msgstr "Grupos"

#: links.py:42 views.py:135
msgid "Create new user"
msgstr "Criar novo Usuário"

#: links.py:65 links.py:74
msgid "Set password"
msgstr "Configurar senha"

#: links.py:70
msgid "User options"
msgstr ""

#: models.py:13
msgid "User"
msgstr "Usuário"

#: models.py:17
msgid "Forbid this user from changing their password."
msgstr ""

#: models.py:23
msgid "User settings"
msgstr ""

#: models.py:24
msgid "Users settings"
msgstr ""

#: permissions.py:10
msgid "Create new groups"
msgstr "Criar novos grupos"

#: permissions.py:13
msgid "Delete existing groups"
msgstr "Excluir grupos existentes"

#: permissions.py:16
msgid "Edit existing groups"
msgstr "Editar grupos existentes"

#: permissions.py:19
msgid "View existing groups"
msgstr "Ver grupos existentes"

#: permissions.py:22
msgid "Create new users"
msgstr "Criar novos usuários"

#: permissions.py:25
msgid "Delete existing users"
msgstr "Excluir usuários existentes"

#: permissions.py:28
msgid "Edit existing users"
msgstr "Editar usuários existentes"

#: permissions.py:31
msgid "View existing users"
msgstr "Ver os usuários existentes"

#: search.py:19
msgid "First name"
msgstr ""

#: search.py:28
msgid "Last name"
msgstr ""

#: search.py:31
msgid "username"
msgstr ""

#: search.py:41
msgid "Name"
msgstr "Nome"

#: serializers.py:34
msgid "Comma separated list of group primary keys to assign this user to."
msgstr "Lista separada por vírgulas de chaves primárias de grupo para atribuir esse usuário."

#: serializers.py:64
msgid "List of group primary keys to which to add the user."
msgstr "Lista de chaves primárias do grupo às quais adicionar o usuário."

#: views.py:48
#, python-format
msgid "Edit group: %s"
msgstr "Editar grupo:%s"

#: views.py:64
msgid ""
"User groups are organizational units. They should mirror the organizational "
"units of your organization. Groups can't be used for access control. Use "
"roles for permissions and access control, add groups to them."
msgstr ""

#: views.py:70
msgid "There are no user groups"
msgstr ""

#: views.py:83
#, python-format
msgid "Delete the group: %s?"
msgstr "Apagar o grupo: %s?"

#: views.py:89
msgid "Available users"
msgstr "Usuários disponíveis"

#: views.py:90
msgid "Users in group"
msgstr ""

#: views.py:111
#, python-format
msgid "Users of group: %s"
msgstr ""

#: views.py:145
#, python-format
msgid "User \"%s\" created successfully."
msgstr "Usuário \"%s\" criado com sucesso."

#: views.py:157
#, python-format
msgid "User delete request performed on %(count)d user"
msgstr "Solicitação de exclusão do usuário executada em %(count)d usuário"

#: views.py:159
#, python-format
msgid "User delete request performed on %(count)d users"
msgstr "Solicitação de exclusão do usuário executada em %(count)d usuários"

#: views.py:167
msgid "Delete user"
msgid_plural "Delete users"
msgstr[0] "Excluir usuários"
msgstr[1] "Excluir usuários"

#: views.py:177
#, python-format
msgid "Delete user: %s"
msgstr "Excluir usuário: %s"

#: views.py:189
msgid ""
"Super user and staff user deleting is not allowed, use the admin interface "
"for these cases."
msgstr "Excluir super usuário e usuário pessoal não é permitido, use a interface de administração para esses casos."

#: views.py:197
#, python-format
msgid "User \"%s\" deleted successfully."
msgstr "Usuário \"%s\" removido com sucesso."

#: views.py:203
#, python-format
msgid "Error deleting user \"%(user)s\": %(error)s"
msgstr "Erro ao excluir usuário \"%(user)s\": %(error)s "

#: views.py:219
#, python-format
msgid "Edit user: %s"
msgstr "Editar usuário:%s"

#: views.py:225
msgid "Available groups"
msgstr "Grupos disponíveis"

#: views.py:226
msgid "Groups joined"
msgstr "Grupos ingressados"

#: views.py:235
#, python-format
msgid "Groups of user: %s"
msgstr "Grupos de usuário:%s"

#: views.py:270
msgid ""
"User accounts can be create from this view. After creating an user account "
"you will prompted to set a password for it. "
msgstr ""

#: views.py:273
msgid "There are no user accounts"
msgstr ""

#: views.py:290
#, python-format
msgid "Edit options for user: %s"
msgstr ""

#: views.py:312
#, python-format
msgid "Password change request performed on %(count)d user"
msgstr "Solicitação de alteração de senha executada em %(count)d usuário"

#: views.py:314
#, python-format
msgid "Password change request performed on %(count)d users"
msgstr "Solicitação de alteração de senha realizada em %(count)d usuários"

#: views.py:321
msgid "Submit"
msgstr "Enviar"

#: views.py:323
msgid "Change user password"
msgid_plural "Change users passwords"
msgstr[0] "Alterar senhas de usuários"
msgstr[1] "Alterar senhas de usuários"

#: views.py:333
#, python-format
msgid "Change password for user: %s"
msgstr "Alterar senha para o usuário: %s"

#: views.py:354
msgid ""
"Super user and staff user password reseting is not allowed, use the admin "
"interface for these cases."
msgstr "Redefinir senha de super usuário e usuário pessoal não é permitido, use a interface de administração para esses casos."

#: views.py:364
#, python-format
msgid "Successful password reset for user: %s."
msgstr ""

#: views.py:370
#, python-format
msgid "Error reseting password for user \"%(user)s\": %(error)s"
msgstr "Erro de redefinição de senha para o usuário \"%(user)s\": %(error)s "
